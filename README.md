<h1 align="center">Hi 👋, I'm Alexandra, a Javascript Developer :)</h1>
<p align="left"> <img src="https://komarev.com/ghpvc/?username=Alexandra2888t&label=Profile%20views&color=0e75b6&style=flat" alt="Alexandra2888" /> </p>

- 🌱 I’m currently learning NextJS.
- 👯 I’m looking to contribute on open source projects.
- 💬 Ask me anything about MERN/MEVN.
- 📫 How to reach me: moldovan.alexandra28@gmail.com

<!-- <div align="center"> -->
<!-- 
## 📈 Stats ~ 

<p align="center" style="display:flex;">

<img width="48%" height="25%" src="https://github-readme-stats.vercel.app/api?username=Alexandra2888&show_icons=true&theme=dark#gh-dark-mode-only" /> 

  <img width="48%"  height="25%" src="https://github-readme-streak-stats.herokuapp.com?user=Alexandra2888&theme=dark&border_radius=10&date_format=M%20j%5B%2C%20Y%5D" />
</p>  -->




## <h1>Languages and Tools<h1>
<p align="center">
  <a href="https://skillicons.dev">
   <img src="https://skillicons.dev/icons?i=nextjs,react,vue,vite,nodejs,html,css,git,mongodb,firebase,js,ts,figma,github,vscode,tailwind,bootstrap&perline=14"/>
  </a>
</p>




 <h2>Hacktoberfest</h2>
 
[![An image of @alexandra2888's Holopin badges, which is a link to view their full Holopin profile](https://holopin.me/alexandra2888)](https://holopin.io/@alexandra2888)




<!--
**Alexandra2888/Alexandra2888** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
